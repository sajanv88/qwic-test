# qwic-test

This is a coding test for a job.

## Installing

If you want to install this:

```
git clone https://sajanv88@bitbucket.org/sajanv88/qwic-test.git

npm install
```

## Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode.

```
curl -X POST \
  http://localhost:8080/api/production_cycle \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 6499f715-2886-4037-8b6c-320f9596dc21' \
  -H 'cache-control: no-cache' \
  -d '[
    {
        "startingDay": "2019-06-03T00:00:00.000Z",
        "duration": 5
    },
    {
        "startingDay": "2019-06-09T00:00:00.000Z",
        "duration": 2
    },
    {
        "startingDay": "2019-06-24T00:00:00.000Z",
        "duration": 5
    },
    {
        "startingDay": "2019-06-16T00:00:00.000Z",
        "duration": 9
    },
    {
        "startingDay": "2019-06-11T00:00:00.000Z",
        "duration": 6
    }
]'
response: 
{
  "productionCycle": 4
}
```

### `npm run prod`

Runs the app in the prod mode.