export interface CycleProduction {
  startingDay:Date;
  duration:number;
}

export interface ProductionList {
  cycleData: CycleProduction[]
}

const getEndDate = (cycleProduction: CycleProduction) => {
  const date:Date = new Date(cycleProduction.startingDay);
  date.setDate(date.getDate() + cycleProduction.duration);
  return date;
}

const isConflict = (period1:CycleProduction, period2:CycleProduction) => {
  const endingDate1:Date = getEndDate(period1);
  const startDate1:Date = new Date(period1.startingDay);

  const endingDate2:Date = getEndDate(period2);
  const startDate2:Date = new Date(period2.startingDay);
  //This is the conflicts
  if(startDate2.getTime() < endingDate1.getTime()) {
    if(endingDate2.getTime() >= endingDate1.getTime() || startDate2.getTime() >= startDate1.getTime()) {
      return true;
    }
  }
  //This is the conflicts
  if(startDate1.getTime() < endingDate2.getTime()) {
    if(endingDate1.getTime() >= endingDate2.getTime() || startDate1.getTime() >= startDate2.getTime()) {
      return true;
    }
  }
  //There is no conflict
  return false;
}

const analyseProductionCycle = (data: ProductionList) => {

  const productionQuantityMax:Number = 100000;
  const productionDurationMax:Number = 1000;

  return new Promise((resolve, reject) => {
    //constraints => 
    //process the data for only 
    //if starting day is greater than current date and the production duration
    const filteredCycleList: CycleProduction[] = data.cycleData.filter(
      (item: CycleProduction) => {
        return new Date(item.startingDay).getTime() > new Date().getTime() && item.duration < productionDurationMax
      }
    );

    const sorted: CycleProduction[] = filteredCycleList.sort(
      (a: CycleProduction, b: CycleProduction) => {
        if(a.startingDay === b.startingDay) {
          return a.duration > b.duration ? 1 : -1
        }
        return a.startingDay > b.startingDay ? 1 : -1;
      }
    );

    if(sorted.length !== 0 && sorted.length < productionQuantityMax) {
      const noConflicts:CycleProduction[] = [];
      const conflicts:CycleProduction[] = [];
      if(sorted.length > 1) {
        sorted.forEach((data: CycleProduction, index: number) => {
          const period = sorted[index + 1];
          if(period) {
            if(!isConflict(data, period)) {
              noConflicts.push(data);
            }else{
              conflicts.push(period);
            }
          }
        });
        return resolve({productionCycle: noConflicts.length + conflicts.length});
      }else {
        return resolve({productionCycle: sorted.length});
      }
    }
    return reject('unable to analyse production cycle');
  });
}

export default analyseProductionCycle; 