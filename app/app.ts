import express,{
  Response,
  Request
} from 'express';
import { json } from 'body-parser';
import analyseProductionCycle, {
  ProductionList,
  CycleProduction
} from './supply-chain';

const PORT = process.env.PORT || 8080;
const app: express.Application = express();
const router: express.Router = express.Router();

app.use(json());
app.use('/api', router);
app.get('/', (req: Request, res: Response) => {
  res.send('QWIC Production api');
});

//just an information route.. 
router.get('/', (req: Request, res: Response) => {
  res.json({
    path: req.url,
    value: 'root path',
    name: 'Qwic API'
  })
});

router.post('/production_cycle', async (req: Request, res: Response) => {
  try{
    const data:ProductionList = {
      cycleData:req.body as CycleProduction[]
    };
    const result = await analyseProductionCycle(data);
    res.status(200).json(result);
  }catch(err) {
    res.status(500).json({info: err});
  }
});

app.listen(PORT,  () => {
  console.log(`Example app listening on port !${PORT}`);
});
